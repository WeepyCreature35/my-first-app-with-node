const sumar = require("../index");
const assert=require("assert");

//Afirmación
describe("Probart la suma de dos numeros", ()=>{
	//Afirmar que cinco más cinco es diez
	it('Cinco mas cinco es diez', ()=>{
		assert.equal(10,sumar(5,5));
	})

	// Afirmar que cinco más cinco no son cincuenta y cinco
	it("Afirmar que cinco más cinco no son cincuenta",()=>{
	   assert.notEqual("55",sumar(5,5));
	});
});
